<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: Header</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/mainStyle.css" />
	</head>
	<body>
		<!-- kodowanie; dzięki temu polskie znaki-->
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		
		<!-- importowanie JSTL -->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		
		<br/>
		<hr/>
		<!-- dane autora -->
		<p><u><c:out value="O autorze:" /></u></p>
		<b><c:out value="Imię i nazwisko:"/></b>
		<c:out value="Mariusz Mazepa" /><br/>
		<b><c:out value="Numer indeksu:" /></b>
		<c:out value="235371" />
	</body>
</html>
