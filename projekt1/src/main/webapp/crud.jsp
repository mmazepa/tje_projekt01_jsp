<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: CRUD</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/mainStyle.css" />
		<link type="text/css" rel="stylesheet" href="css/tableStyle.css" />
	</head>
	<body>
		<!-- importowanie klas z własnej paczki "myPackage" -->
		<%@ page import="myPackage.Zwierze" %>
		<%@ page import="myPackage.CzerwonaKsiega" %>

		<!-- importowanie pozostałych klas użytych w dokumencie -->
		<%@ page import="java.util.TreeMap" %>
		<%@ page import="java.util.Map" %>

		<!-- kodowanie; dzięki temu polskie znaki-->
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		
		<!-- importowanie JSTL -->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
		<jsp:include page="header.jsp" />
		<h3><c:out value="CRUD" /></h3>
		<p><c:out value="Zobacz, dodaj, zmień lub usuń!" /></p>
		<br/>
		<!-- wyświetlanie zwierząt - oprawa graficzna (tabelka)  -->
		<table>
			<caption>
				<c:out value="Zwierzęta w Polsce" />
			</caption>
			<tr id="zwierze1">
				<th><c:out value="Identyfikator" /></th>
				<th><c:out value="Gatunek" /></th>
				<th><c:out value="Imię" /></th>
				<th><c:out value="CKGZ" /></th>
				<th><c:out value="Usuń" /></th>
			</tr>
			<%
				int idInt = 0;
				TreeMap<String, Zwierze> zwierzMap = new Zwierze().getZwierzeta();
				for(Map.Entry<String,Zwierze> entry : zwierzMap.entrySet()){
			%>
					<tr id='zwierze2'>
						<th><% out.println(entry.getKey()); %></th>
						<%
							idInt = Integer.parseInt(entry.getKey().substring(6));
							idInt++;
						%>
						<th><% out.println(entry.getValue().getGatunek()); %></th>
						<th><% out.println(entry.getValue().getImie()); %></th>
						<th><% out.println(entry.getValue().getCKGZ()); %></th>
						<!-- usuwanie; przycisk "usuń" obok każdego zwierzęcia -->
						<th>
							<form method="POST">
								<input	type="submit"
										name="<%out.print(entry.getKey());%>"
										value="<%out.print(entry.getKey());%>"
										id="<%out.print(entry.getKey());%>" />
							</form>
						</th>
						<%
							String rightId = request.getParameter(entry.getKey());
							if(request.getParameter(rightId) != null){
								zwierzMap.remove(entry.getKey());
								response.sendRedirect("crud.jsp");
							}
						%>
					<tr>
			<%
				}
			%>
		</table>
		<br/>
		<table>
			<tr id="zwierze3">
				<th>
					<!-- dodawanie zwierząt do tabeli -->
					<p><c:out value="Dodaj nowe zwierzę:" /></p>
					<form method="POST">
						<input type="text" name="gatunek" id="gatunek" placeholder="Podaj gatunek..."><br/>
						<input type="text" name="imie" id="imie" placeholder="Podaj imię..."><br/>
						<c:out value="Stopień zagrożenia:" />
						<select name="ckgz">
							<%
								TreeMap<String, CzerwonaKsiega> ksiegaMap = new CzerwonaKsiega().getZagrozenia();
								for(Map.Entry<String,CzerwonaKsiega> entry : ksiegaMap.entrySet()){
									out.println("<option>" + entry.getValue().getSymbol() + "</option>");
								}
							%>
						</select><br/>
						<input type="submit" name="subDodaj" value="dodaj">
						<br/>
						<br/>
					</form>
					<%
						if(request.getParameter("subDodaj") != null){
							Zwierze newZwierze = new Zwierze(request.getParameter("gatunek"), request.getParameter("imie"), request.getParameter("ckgz"));
							if(newZwierze.getGatunek() != null && newZwierze.getGatunek() != "" && newZwierze.getImie() != null && newZwierze.getImie() != "" && newZwierze.getCKGZ() != null && newZwierze.getCKGZ() != ""){
								zwierzMap.put("zwierz" + idInt++, newZwierze);
								response.sendRedirect("crud.jsp");
							}
						}
					%>
				</th>
				<th>
					<!-- modyfikacja zwierząt z tabeli -->
					<p><c:out value="Zmodyfikuj zwierzę:" /></p>
					<form method="POST">
						<c:out value="Które?" />
						<select name="ktoryZwierz">
							<%
								for(Map.Entry<String,Zwierze> entry : zwierzMap.entrySet()){
									out.println("<optgroup label='" + entry.getValue().getGatunek() + " " + entry.getValue().getImie() + "'>");
									out.println("<option>" + entry.getKey() + "</option>");
									out.println("</optgroup>");
								}
							%>
						</select><br/>
						<input type="text" name="gatunek2" id="gatunek2" placeholder="Podaj gatunek..."><br/>
						<input type="text" name="imie2" id="imie2" placeholder="Podaj imię..."><br/>
						<c:out value="Stopień zagrożenia:" />
						<select name="ckgz2">
							<%
								for(Map.Entry<String,CzerwonaKsiega> entry : ksiegaMap.entrySet()){
									out.println("<option>" + entry.getValue().getSymbol() + "</option>");
								}
							%>
						</select><br/>
						<input type="submit" name="subZmien" value="zmień">
						<br/>
						<br/>
					</form>
					<%
						String ktoryZwierz = request.getParameter("ktoryZwierz");
						if(request.getParameter("subZmien") != null){
							Zwierze newZwierze = new Zwierze(request.getParameter("gatunek2"), request.getParameter("imie2"), request.getParameter("ckgz2"));
							if(newZwierze.getGatunek() != null && newZwierze.getGatunek() != "" && newZwierze.getImie() != null && newZwierze.getImie() != "" && newZwierze.getCKGZ() != null && newZwierze.getCKGZ() != ""){
								zwierzMap.put(ktoryZwierz, newZwierze);
								response.sendRedirect("crud.jsp");
							}
						}
					%>
				</th>
			</tr>
		</table>
		<br/>
		<br/>
		<jsp:useBean id="zwierzak1" class="myPackage.Zwierze">
			<jsp:setProperty name="zwierzak1" property="gatunek" value="Żubr"/>
			<jsp:setProperty name="zwierzak1" property="imie" value="Kazimierz"/>
			<jsp:setProperty name="zwierzak1" property="CKGZ" value="VU"/>
		</jsp:useBean>
		<jsp:useBean id="zwierzak2" class="myPackage.Zwierze">
			<jsp:setProperty name="zwierzak2" property="gatunek" value="Kosarz"/>
			<jsp:setProperty name="zwierzak2" property="imie" value="Henryk"/>
			<jsp:setProperty name="zwierzak2" property="CKGZ" value="-"/>
		</jsp:useBean>
		<jsp:useBean id="zwierzak3" class="myPackage.Zwierze">
			<jsp:setProperty name="zwierzak3" property="gatunek" value="Żółw"/>
			<jsp:setProperty name="zwierzak3" property="imie" value="Andrzej"/>
			<jsp:setProperty name="zwierzak3" property="CKGZ" value="NT"/>
		</jsp:useBean>
		<table>
			<caption>
				<c:out value="Przykładowa tabelka ze zwierzętami" />
			</caption>
			<tr id="zwierze1">
				<th><c:out value="Gatunek" /></th>
				<th><c:out value="Imię" /></th>
				<th><c:out value="CKGZ" /></th>
			</tr>
			<tr id="zwierze2">
				<th><jsp:getProperty name="zwierzak1" property="gatunek"/></th>
				<th><jsp:getProperty name="zwierzak1" property="imie"/></th>
				<th><jsp:getProperty name="zwierzak1" property="CKGZ"/></th>
			</tr>
			<tr id="zwierze2">
				<th><jsp:getProperty name="zwierzak2" property="gatunek"/></th>
				<th><jsp:getProperty name="zwierzak2" property="imie"/></th>
				<th><jsp:getProperty name="zwierzak2" property="CKGZ"/></th>
			</tr>
			<tr id="zwierze2">
				<th><jsp:getProperty name="zwierzak3" property="gatunek"/></th>
				<th><jsp:getProperty name="zwierzak3" property="imie"/></th>
				<th><jsp:getProperty name="zwierzak3" property="CKGZ"/></th>
			</tr>
		</table>
		<br/>
		<br/>
		<!-- wyświetlanie Czerwonej Księgi - oprawa graficzna (tabelka) -->
		<table>
		<caption>
			<c:out value="Czerwona Księga Gatunków Zagrożonych (CKGZ)" />
		</caption>
			<tr id="ksiega1">
				<th><c:out value="Identyfikator" /></th>
				<th><c:out value="Symbol" /></th>
				<th><c:out value="Znaczenie" /></th>
			</tr>
		<%
			for(Map.Entry<String,CzerwonaKsiega> entry : ksiegaMap.entrySet()){
		%>
				<tr id='ksiega2'>
					<th><% out.println(entry.getKey()); %></th>
					<th><% out.println(entry.getValue().getSymbol()); %></th>
					<th><% out.println(entry.getValue().getZnaczenie()); %></th>
				<tr>
		<%	
			}
		%>
		</table>
		<jsp:include page="footer.jsp" />
	</body>
</html>
