<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: Servlet</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/mainStyle.css" />
	</head>
	<body>
		<!-- importowanie klas z własnej paczki "myPackage" -->
		<%@ page import="myPackage.Zwierze" %>
		<%@ page import="myPackage.CzerwonaKsiega" %>
		
		<!-- importowanie pozostałych klas użytych w dokumencie -->
		<%@ page import="java.util.TreeMap" %>
		<%@ page import="java.util.Map" %>
		
		<!-- kodowanie; dzięki temu polskie znaki-->
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		
		<!-- importowanie JSTL -->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		
		<jsp:include page="header.jsp" />
		<h3><c:out value="Servlet" /></h3>
		<%
			TreeMap<String, Zwierze> zwierzMap = new Zwierze().getZwierzeta();
			int licznik = 1;
		%>
		<u><c:out value="Zarejestrowane w bazie zwierzęta to:" /></u><br/>
		<%
			for(Map.Entry<String,Zwierze> entry : zwierzMap.entrySet()){
				out.println("&nbsp;&nbsp;&nbsp;" + licznik++ + ") " + entry.getValue().getGatunek() + " o imieniu " + entry.getValue().getImie() + "<br/>");
			}
		%>
		<br/>
		<c:out value="Wynika z tego, że mamy w bazie dokładnie" />
		<%
			out.println(zwierzMap.size());
		%>
		<c:out value="zwierząt." /><br/>
		<br/>
		<hr>
		<h4>
			<c:out value="Sprawdź w bazie..." />
		</h4>
		<form method="GET" action="simple">
			<input type="text" name="gatunek" id="gatunek" placeholder="Wpisz gatunek..." />
			<input type="submit" value="sprawdź" />
		</form>
		<%
			if(request.getAttribute("informacja") != null && request.getAttribute("informacja") != ""){
				out.println("<h3>" + request.getAttribute("informacja") + "</h3>");
				if(request.getAttribute("postscriptum") != null && request.getAttribute("postscriptum") != ""){
					out.println("<p>" + request.getAttribute("postscriptum") + "</p>");
				}
			}
		%>
		<br/>
		<hr>
		<h4>
			<c:out value="Kategorie w Czerwonej Księdze - co oznaczają?" />
		</h4>
		<form method="GET" action="simple">
			<b><c:out value="Symbol:" /></b>
			<select name="ckgz">
				<%
					TreeMap<String, CzerwonaKsiega> ksiegaMap = new CzerwonaKsiega().getZagrozenia();
					for(Map.Entry<String,CzerwonaKsiega> entry : ksiegaMap.entrySet()){
						out.println("<option>" + entry.getValue().getSymbol() + "</option>");
					}
				%>
			</select>
			<input type="submit" value="sprawdź"/>
			<br/>
			<b><c:out value="Znaczenie:" /></b>
			<%
				if(request.getAttribute("definicja") != null && request.getAttribute("definicja") != ""){
					out.println("<p>" + request.getAttribute("definicja") + "</p>");
				}
			%>
		</form>
		<jsp:include page="footer.jsp" />
	</body>
</html>
