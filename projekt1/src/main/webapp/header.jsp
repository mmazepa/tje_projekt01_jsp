<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: Header</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/mainStyle.css" />
	</head>
	<body>
		<!-- kodowanie; dzięki temu polskie znaki-->
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		
		<!-- importowanie JSTL -->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		
		<jsp:useBean id="date" class="java.util.Date" />
		<p>
			<b>
				<jsp:expression>date</jsp:expression>
			</b>
			<br/>
			<c:out value="Minęło już" />
			<jsp:expression> 
				date.getHours()
			</jsp:expression>
			<c:out value="godzin," />
			<jsp:expression>
				date.getMinutes()
			</jsp:expression>
			<c:out value="minut i" />
			<jsp:expression>
				date.getSeconds()
			</jsp:expression>
			<c:out value="sekund dzisiejszego dnia." />
		</p>
		<a id="goBack" href="index.jsp">
			<c:out value="Strona Główna" />
		</a>
		<hr/>
	</body>
</html>
