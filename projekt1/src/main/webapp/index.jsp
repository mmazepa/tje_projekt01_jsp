<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: Project01</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/mainStyle.css" />
	</head>
	<body>
		<!-- importowanie klas użytych w dokumencie -->
		<%@ page import="java.util.Calendar" %>
		
		<!-- kodowanie; dzięki temu polskie znaki-->
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		
		<!-- importowanie JSTL -->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		
		<jsp:include page="header.jsp" />
		<h3>
		<%
			int hourNow = java.util.Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			if(hourNow >= 4 && hourNow < 18){
		%>
			<c:out value="Dzień dobry!" />
		<%
			} else if(hourNow >= 18 && hourNow < 22){
		%>
				<c:out value="Dobry wieczór!" />
		<%
				} else if(hourNow >= 22 || hourNow < 4){
		%>
					<c:out value="Dobranoc!" />
		<%
					}
		%>
		</h3>
		<p><c:out value="Ładny mamy dziś dzień." /></p>
		<!-- menu główne -->
		<a href="hello.jsp"><c:out value="01) Hello" /></a><br/>
		<a href="crud.jsp"><c:out value="02) CRUD" /></a><br/>
		<a href="servlet.jsp"><c:out value="03) Servlet" /></a><br/>
		<jsp:include page="footer.jsp" />
	</body>
</html>
