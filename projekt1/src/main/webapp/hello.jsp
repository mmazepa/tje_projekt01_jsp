<!DOCTYPE HTML>
<html>
	<head>
		<title>TJE: Hello</title>
		<meta charset="UTF-8" />
		<link type="text/css" rel="stylesheet" href="css/mainStyle.css" />
	</head>
	<body>		
		<!-- kodowanie; dzięki temu polskie znaki-->
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>
		
		<!-- importowanie JSTL -->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		
		<jsp:include page="header.jsp" />
		<%
			out.println("<b>Twój adres IP:</b> " + request.getRemoteAddr());
		%>
		<br/>
		<br/>
		<form method="GET" action="welcome">
			<label for="name">
				<c:out value="Podaj swoje imię..." />
			</label><br/>
			<input type="text" name="name" id="name" placeholder="Wpisz imię..." />
			<input type="submit" value="Kliknij">
		</form>
		<%-- efekt działania servletu WelcomeServlet --%>
		<%
			if(request.getAttribute("powitanie") != null && request.getAttribute("powitanie") != ""){
				out.println("<h3>" + request.getAttribute("powitanie") + "</h3>");
				if(request.getAttribute("dopisek") != null && request.getAttribute("dopisek") != ""){
					out.println("<p>" + request.getAttribute("dopisek") + "</p>");
				}
			}
		%>
		<jsp:include page="footer.jsp" />
	</body>
</html>
