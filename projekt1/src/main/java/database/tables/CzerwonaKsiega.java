//klasa "CzerwonaKsiega" do opisu stopni zagrożenia
//jedna z tabel potrzebna do bazy danych

package myPackage;

import java.util.Map;
import java.util.TreeMap;

public class CzerwonaKsiega {
	
	private String symbol = new String();
	private String znaczenie = new String();
	
	public CzerwonaKsiega(){
		super();
	}
	
	public CzerwonaKsiega(String symbol, String znaczenie){
		super();
		this.symbol = symbol;
		this.znaczenie = znaczenie;
	}
	
	public String getSymbol(){
		return symbol;
	}
	
	public void setSymbol(String symbol){
		this.symbol = symbol;
	}
	
	public String getZnaczenie(){
		return znaczenie;
	}
	
	public void setZnaczenie(String znaczenie){
		this.znaczenie = znaczenie;
	}
	
	private static final TreeMap<String, CzerwonaKsiega> stopienZagrozenia;
	static {
		TreeMap<String, CzerwonaKsiega> aMap = new TreeMap<String, CzerwonaKsiega>();
		
		CzerwonaKsiega stopien1 = new CzerwonaKsiega();
		stopien1.setSymbol("EX");
		stopien1.setZnaczenie("Wymarłe (extinct)");
		aMap.put("stopien1", stopien1);
		
		CzerwonaKsiega stopien2 = new CzerwonaKsiega();
		stopien2.setSymbol("EW");
		stopien2.setZnaczenie("Wymarłe na wolności (extinct in the wild)");
		aMap.put("stopien2", stopien2);
		
		CzerwonaKsiega stopien3 = new CzerwonaKsiega();
		stopien3.setSymbol("CR");
		stopien3.setZnaczenie("Krytycznie zagrożone (critically endangered)");
		aMap.put("stopien3", stopien3);
		
		CzerwonaKsiega stopien4 = new CzerwonaKsiega();
		stopien4.setSymbol("EN");
		stopien4.setZnaczenie("Zagrożone (endangered)");
		aMap.put("stopien4", stopien4);
		
		CzerwonaKsiega stopien5 = new CzerwonaKsiega();
		stopien5.setSymbol("VU");
		stopien5.setZnaczenie("Narażone (vulnerable)");
		aMap.put("stopien5", stopien5);
		
		CzerwonaKsiega stopien6 = new CzerwonaKsiega();
		stopien6.setSymbol("NT");
		stopien6.setZnaczenie("Bliskie zagrożenia (near threatened)");
		aMap.put("stopien6", stopien6);
		
		CzerwonaKsiega stopien7 = new CzerwonaKsiega();
		stopien7.setSymbol("LC");
		stopien7.setZnaczenie("Najmniejszej troski (least concern)");
		aMap.put("stopien7", stopien7);
		
		CzerwonaKsiega stopien8 = new CzerwonaKsiega();
		stopien8.setSymbol("-");
		stopien8.setZnaczenie("Nie objęte ochroną");
		aMap.put("stopien8", stopien8);
		
		stopienZagrozenia = aMap;
	}
	
	public static TreeMap<String, CzerwonaKsiega> getZagrozenia(){
		return stopienZagrozenia;
	}
	
	public static void main(String[] args){
		getZagrozenia();
	}
}
