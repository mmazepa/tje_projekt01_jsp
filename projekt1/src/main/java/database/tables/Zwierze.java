//klasa "Zwierze" do opisu konkretnego zwierza
//jedna z tabel potrzebna do bazy danych

package myPackage;

import java.util.Map;
import java.util.TreeMap;

public class Zwierze {
	
	private String gatunek = new String();
	private String imie = new String();
	private String ckgz = new String();
	
	public Zwierze(){
		super();
	}
	
	public Zwierze(String gatunek, String imie, String ckgz){
		super();
		this.gatunek = gatunek;
		this.imie = imie;
		this.ckgz = ckgz;
	}
	
	public String getGatunek(){
		return gatunek;
	}
	
	public void setGatunek(String gatunek){
		this.gatunek = gatunek;
	}
	
	public String getImie(){
		return imie;
	}
	
	public void setImie(String imie){
		this.imie = imie;
	}
	
	public String getCKGZ(){
		return ckgz;
	}
	
	public void setCKGZ(String ckgz){
		this.ckgz = ckgz;
	}
	
	private static final TreeMap<String, Zwierze> zwierz;
	static {
		TreeMap<String, Zwierze> aMap = new TreeMap<String, Zwierze>();
		
		Zwierze zwierz1 = new Zwierze();
		zwierz1.setGatunek("Żubr");
		zwierz1.setImie("Ryszard");
		zwierz1.setCKGZ("VU");
		aMap.put("zwierz1", zwierz1);
		
		Zwierze zwierz2 = new Zwierze();
		zwierz2.setGatunek("Jeż");
		zwierz2.setImie("Karol");
		zwierz2.setCKGZ("LC");
		aMap.put("zwierz2", zwierz2);
		
		Zwierze zwierz3 = new Zwierze();
		zwierz3.setGatunek("Bóbr");
		zwierz3.setImie("Eryk");
		zwierz3.setCKGZ("LC");
		aMap.put("zwierz3", zwierz3);
		
		Zwierze zwierz4 = new Zwierze();
		zwierz4.setGatunek("Nietoperz");
		zwierz4.setImie("Janusz");
		zwierz4.setCKGZ("LC");
		aMap.put("zwierz4", zwierz4);
		
		Zwierze zwierz5 = new Zwierze();
		zwierz5.setGatunek("Lis");
		zwierz5.setImie("Albert");
		zwierz5.setCKGZ("LC");
		aMap.put("zwierz5", zwierz5);
		
		zwierz = aMap;
	}
	
	public static TreeMap<String, Zwierze> getZwierzeta(){
		return zwierz;
	}
	
	public static void main(String[] args){
		getZwierzeta();
	}
}
