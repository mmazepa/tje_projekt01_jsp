import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.TreeMap;
import java.util.Map;

import myPackage.Zwierze;
import myPackage.CzerwonaKsiega;

public class SimpleServlet extends HttpServlet{
	public void doGet(HttpServletRequest request,
	HttpServletResponse response)
	throws ServletException, IOException {
		TreeMap<String, Zwierze> zwierzMap = (TreeMap<String, Zwierze>)request.getAttribute("zwierzMap");
		for(Map.Entry<String, Zwierze> entry : zwierzMap.entrySet()){
			if(request.getParameter("gatunek").equals(entry.getValue().getGatunek())){
				request.setAttribute("informacja", "Tak, " + request.getParameter("gatunek") + " jest w bazie.");
				request.setAttribute("postscriptum", "Wybornie!");
			}
			else {
				request.setAttribute("informacja", "Niestety, " + request.getParameter("gatunek") + " nie znajduje się w bazie.");
				request.setAttribute("postscriptum", "Ale zawsze możesz <a href='crud.jsp'>dodać</a> "  + request.getParameter("gatunek") + " do bazy.");
			}	
		}
		
		TreeMap<String, CzerwonaKsiega> ksiegMap = (TreeMap<String, CzerwonaKsiega>)request.getAttribute("ksiegMap");
		for(Map.Entry<String, CzerwonaKsiega> entry : ksiegMap.entrySet()){
			if(request.getParameter("ckgz") != null && request.getParameter("ckgz") != ""){
				request.setAttribute("definicja", "Definicja.");
			}
			else {
				request.setAttribute("definicja", "Brak definicji.");
			}
		}
		
		getServletContext().getRequestDispatcher("/servlet.jsp").forward(request, response);
	}
}
