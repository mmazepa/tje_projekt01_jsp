import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WelcomeServlet extends HttpServlet{
	protected void doGet(HttpServletRequest request,
	HttpServletResponse response)
	throws ServletException, IOException {
		if(request.getParameter("name") != null && request.getParameter("name") != ""){
			request.setAttribute("powitanie", "Witaj, " + request.getParameter("name") + "!");
			request.setAttribute("dopisek", "Miło Cię poznać!");
		}
		else {
			request.setAttribute("powitanie", "Witaj, anonimowy gościu!");
			request.setAttribute("dopisek", "Dlaczego nie chcesz się przedstawić?");
		}
		getServletContext().getRequestDispatcher("/hello.jsp").forward(request, response);
	}
}
